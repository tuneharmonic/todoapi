﻿using System;
using System.Collections.Generic;
using System.Linq;
using TodoApi.DAL;
using TodoApi.Model;

namespace TodoApi.BLL
{
    public class TodoLogic
    {
        private ITodoItemStore itemStore;
        private ITodoListStore listStore;

        public TodoLogic(ITodoItemStore itemStore, ITodoListStore listStore)
        {
            this.itemStore = itemStore;
            this.listStore = listStore;
        }

        public IEnumerable<TodoItemVM> GetAllTodoItems() => 
            itemStore.GetTodoItems()?.Select(ti => Mapper.ItemToVm(ti));

        public TodoItemVM GetTodoItem(int id) => 
            Mapper.ItemToVm(itemStore.GetTodoItem(id));

        public TodoItemVM AddTodoItem(TodoItem item)
        {
            var validation = ValidateTodoItem(item, true);
            if (validation.Valid)
            {
                var result = itemStore.AddTodoItem(item);
                return Mapper.ItemToVm(result);
            }
            return null;
        }

        public TodoItemVM UpdateTodoItem(TodoItem item)
        {
            var validation = ValidateTodoItem(item);
            var existing = itemStore.GetTodoItem(item.ItemId);
            if (null != existing && validation.Valid)
            {
                existing.Completed = item.Completed;
                existing.Description = item.Description;
                existing.ListId = item.ListId;
                existing.Title = item.Title;
                existing.TodoList = listStore.GetTodoList(item.ListId);

                existing = itemStore.UpdateTodoItem(existing);
            }
            return Mapper.ItemToVm(existing);
        }

        public bool DeleteTodoItem(int id)
        {
            if (id > 0)
            {
                return itemStore.DeleteTodoItem(id);
            }
            return false;
        }

        public IEnumerable<TodoListVM> GetAllTodoLists() =>
            listStore.GetTodoLists().Select(tl => Mapper.ListToVm(tl));

        public TodoListVM GetTodoList(int id) => 
            Mapper.ListToVm(listStore.GetTodoList(id));

        public TodoListVM AddTodoList(string title)
        {
            if (!string.IsNullOrEmpty(title?.Trim()))
            {
                var list = new TodoList { Title = title };
                list = listStore.AddTodoList(list);
                return Mapper.ListToVm(list);
            }
            return null;
        }

        public TodoListVM UpdateTodoList(int id, string title)
        {
            var list = listStore.GetTodoList(id);
            if (null != list && !string.IsNullOrEmpty(title?.Trim()))
            {
                list.Title = title;
                list = listStore.UpdateTodoList(list);
            }
            return Mapper.ListToVm(list);
        }

        public bool DeleteTodoList(int id)
        {
            if (id > 0)
            {
                return listStore.DeleteTodoList(id);
            }
            return false;
        }

        public ValidationResult ValidateTodoItem(TodoItem item, bool newItem = false)
        {
            var errors = new List<string>();

            if (null != item)
            {
                if (newItem && item.ItemId != 0)
                {
                    errors.Add("ItemId should not be set.");
                }
                if (string.IsNullOrEmpty(item.Description?.Trim()))
                {
                    errors.Add("Item Description is required.");
                }
                if (string.IsNullOrEmpty(item.Title?.Trim()))
                {
                    errors.Add("Item Title is required.");
                }
                if (item.ListId == 0)
                {
                    errors.Add("Item must belong to a List.");
                }
            }
            else
            {
                errors.Add("Item is null.");
            }

            return new ValidationResult(!errors.Any(), errors);
        }

        public ValidationResult ValidateTodoList(TodoList list)
        {
            var errors = new List<string>();

            if (null != list)
            {
                if (string.IsNullOrEmpty(list.Title?.Trim()))
                {
                    errors.Add("List Title is required.");
                }
            }
            else
            {
                errors.Add("List is null.");
            }
            
            return new ValidationResult(!errors.Any(), errors);
        }
    }
}
