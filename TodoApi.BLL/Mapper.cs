using System.Linq;
using TodoApi.Model;

namespace TodoApi.BLL
{
    public static class Mapper
    {
        public static TodoItemVM ItemToVm(TodoItem item)
        {
            if (null != item)
            {
                return new TodoItemVM
                {
                    ItemId = item.ItemId,
                    Completed = item.Completed,
                    Description = item.Description,
                    ListId = item.ListId,
                    Title = item.Title
                };
            }
            return null;
        }

        public static TodoListVM ListToVm(TodoList list)
        {
            if (null != list)
            {
                return new TodoListVM
                {
                    ListId = list.ListId,
                    Title = list.Title,
                    TodoItems = list.TodoItems.Select(ti => ItemToVm(ti)).ToList()
                };
            }
            return null;
        }
    }
}