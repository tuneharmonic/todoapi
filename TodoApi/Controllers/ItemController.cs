﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TodoApi.BLL;
using TodoApi.Model;

namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        private TodoLogic logic;
        
        public ItemController(TodoLogic logic)
        {
            this.logic = logic;
        }

        // GET api/item
        [HttpGet]
        public ActionResult<IEnumerable<TodoItemVM>> Get()
        {
            return new JsonResult(logic.GetAllTodoItems());
        }

        // GET api/item/5
        [HttpGet("{id}")]
        public ActionResult<TodoItemVM> Get(int id)
        {
            return new JsonResult(logic.GetTodoItem(id));
        }

        // POST api/item
        [HttpPost]
        public ActionResult<TodoItemVM> Post([FromBody] TodoItem item)
        {
            return new JsonResult(logic.AddTodoItem(item));
        }

        // PUT api/item/5
        [HttpPut]
        public ActionResult<TodoItemVM> Put([FromBody] TodoItem item)
        {
            return new JsonResult(logic.UpdateTodoItem(item));
        }

        // DELETE api/item/5
        [HttpDelete("{id}")]
        public ActionResult<bool> Delete(int id)
        {
            return new JsonResult(logic.DeleteTodoItem(id));
        }
    }
}
