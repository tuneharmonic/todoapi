﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TodoApi.BLL;
using TodoApi.Model;

namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ListController : ControllerBase
    {
        private TodoLogic logic;
        
        public ListController(TodoLogic logic)
        {
            this.logic = logic;
        }

        // GET api/list
        [HttpGet]
        public ActionResult<IEnumerable<TodoListVM>> Get()
        {
            return new JsonResult(logic.GetAllTodoLists());
        }

        // GET api/list/5
        [HttpGet("{id}")]
        public ActionResult<TodoListVM> Get(int id)
        {
            return new JsonResult(logic.GetTodoList(id));
        }

        // POST api/list
        [HttpPost]
        public ActionResult<TodoListVM> Post([FromBody] string title)
        {
            return new JsonResult(logic.AddTodoList(title));
        }

        // PUT api/list/5
        [HttpPut("{id}")]
        public ActionResult<TodoListVM> Put(int id, [FromBody] string title)
        {
            return new JsonResult(logic.UpdateTodoList(id, title));
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public ActionResult<bool> Delete(int id)
        {
            return new JsonResult(logic.DeleteTodoList(id));
        }
    }
}
