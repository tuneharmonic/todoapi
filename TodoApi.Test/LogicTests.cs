using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using TodoApi.BLL;
using TodoApi.DAL;
using TodoApi.Model;

namespace Tests
{
    public class LogicTests
    {
        private TodoLogic GetTestLogic(ITodoItemStore itemStore = null, ITodoListStore listStore  = null)
        {
            var defaultItemStore = new Mock<ITodoItemStore>().Object;
            var defaultListStore = new Mock<ITodoListStore>().Object;

            itemStore = itemStore ?? defaultItemStore;
            listStore = listStore ?? defaultListStore;
            
            return new TodoLogic(itemStore, listStore);
        }

        private TodoList GetTestData()
        {
            var list = new TodoList
            {
                ListId = 1,
                Title = "List 1"
            };

            var items = new List<TodoItem>
            {
                new TodoItem
                {
                    ItemId = 1,
                    Completed = false,
                    Title  = "Item 1",
                    Description = "The first item",
                    ListId = 1,
                    TodoList = list
                },
                new TodoItem
                {
                    ItemId = 2,
                    Completed = true,
                    Title = "Title 2",
                    Description = "This one has a different title!",
                    ListId = 1,
                    TodoList = list
                },
                new TodoItem
                {
                    ItemId = 3,
                    Completed = false,
                    Title = "Run some tests!",
                    Description = "Go on, run 'em! I know you want to...",
                    ListId = 1,
                    TodoList = list
                }
            };

            list.TodoItems = items;

            return list;
        }

        [Test]
        public void GetAllTodoItems_Nice()
        {
            var items = GetTestData().TodoItems;
            var itemStoreMock = new Mock<ITodoItemStore>();
            itemStoreMock.Setup(tis => tis.GetTodoItems())
                .Returns(items.AsEnumerable());
            var logic = GetTestLogic(itemStoreMock.Object);
            var actual = logic.GetAllTodoItems();
            Assert.IsNotNull(actual);
            Assert.AreEqual(3, actual.Count());
        }

        [Test]
        public void GetAllTodoItems_Null()
        {
            List<TodoItem> items = null;
            var itemStoreMock = new Mock<ITodoItemStore>();
            itemStoreMock.Setup(tis => tis.GetTodoItems())
                .Returns(items.AsEnumerable());
            var logic = GetTestLogic(itemStoreMock.Object);
            var actual = logic.GetAllTodoItems();
            Assert.IsNull(actual);
        }

        [Test]
        public void GetTodoItem_Nice()
        {
            var items = GetTestData().TodoItems;
            var itemStoreMock = new Mock<ITodoItemStore>();
            itemStoreMock.Setup(
                    tis => tis.GetTodoItem(It.IsAny<int>())
                )
                .Returns(
                    (int id) => items.SingleOrDefault(i => i.ItemId == id)
                );
            var logic = GetTestLogic(itemStoreMock.Object);
            var actual = logic.GetTodoItem(2);
            Assert.IsNotNull(actual);
            Assert.AreEqual("Title 2", actual.Title);
        }
    }
}