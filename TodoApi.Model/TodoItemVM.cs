namespace TodoApi.Model
{
    public class TodoItemVM
    {
        public int ItemId { get; set; }
        public bool Completed { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int ListId { get; set; }
    }
}