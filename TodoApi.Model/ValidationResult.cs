using System.Collections.Generic;

namespace TodoApi.Model
{
    public class ValidationResult
    {
        public bool Valid { get; set; }
        public List<string> Errors { get; set; }

        public ValidationResult(bool valid = true, List<string> errors = null)
        {
            this.Valid = valid;
            this.Errors = errors;
        }
    }
}