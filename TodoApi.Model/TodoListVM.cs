﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TodoApi.Model
{
    public class TodoListVM
    {
        public int ListId { get; set; }
        public string Title { get; set; }
        public List<TodoItemVM> TodoItems { get; set; }
    }
}
