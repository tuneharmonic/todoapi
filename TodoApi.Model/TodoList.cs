﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TodoApi.Model
{
    public class TodoList
    {
        [Key]
        public int ListId { get; set; }
        public string Title { get; set; }
        
        public List<TodoItem> TodoItems { get; set; }
    }
}
