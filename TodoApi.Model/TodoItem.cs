﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TodoApi.Model
{
    public class TodoItem
    {
        [Key]
        public int ItemId { get; set; }
        public bool Completed { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public int ListId { get; set; }
        public TodoList TodoList { get; set; }
    }
}
