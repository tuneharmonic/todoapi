﻿using System;
using System.Collections.Generic;
using TodoApi.Model;

namespace TodoApi.DAL
{
    public interface ITodoListStore
    {
        IEnumerable<TodoList> GetTodoLists();
        TodoList GetTodoList(int id);
        TodoList AddTodoList(TodoList list);
        TodoList UpdateTodoList(TodoList list);
        bool DeleteTodoList(int id);
    }
}
