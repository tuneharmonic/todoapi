using System;
using Microsoft.EntityFrameworkCore;
using TodoApi.Model;

namespace TodoApi.DAL
{
    public class SqliteContext : TodoContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder contextOptionsBuilder)
        {
            contextOptionsBuilder.UseSqlite("Data Source=todo.db");
        }
    }
}