using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TodoApi.Model;

namespace TodoApi.DAL
{
    public class SqliteTodoItemStore : ITodoItemStore
    {
        private TodoContext context;

        public SqliteTodoItemStore(TodoContext context)
        {
            this.context = context;
        }

        public TodoItem AddTodoItem(TodoItem item)
        {
            var list = context.TodoLists
                .Include(tl => tl.TodoItems)
                .SingleOrDefault(tl => tl.ListId == item.ListId);
            if (null != list)
            {
                list.TodoItems.Add(item);
                if (context.SaveChanges() > 0)
                {
                    return item;
                }
            }
            return null;
        }

        public bool DeleteTodoItem(int id)
        {
            context.Remove<TodoItem>(
                context.TodoItems.SingleOrDefault(
                    ti => ti.ItemId == id
                )
            );
            return context.SaveChanges() > 0;
        }

        public TodoItem GetTodoItem(int id)
        {
            return context.TodoItems.SingleOrDefault(ti => ti.ItemId == id);
        }

        public IEnumerable<TodoItem> GetTodoItems()
        {
            return context.TodoItems.AsEnumerable();
        }

        public TodoItem UpdateTodoItem(TodoItem item)
        {
            context.Attach<TodoItem>(item);
            if (context.SaveChanges() > 0)
            {
                return item;
            }
            return null;
        }
    }
}