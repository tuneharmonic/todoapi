﻿using System;
using System.Collections.Generic;
using TodoApi.Model;

namespace TodoApi.DAL
{
    public interface ITodoItemStore
    {
        IEnumerable<TodoItem> GetTodoItems();
        TodoItem GetTodoItem(int id);
        TodoItem AddTodoItem(TodoItem item);
        TodoItem UpdateTodoItem(TodoItem item);
        bool DeleteTodoItem(int id);
    }
}
