using Microsoft.EntityFrameworkCore;
using TodoApi.Model;

namespace TodoApi.DAL
{
    public abstract class TodoContext : DbContext
    {
        public DbSet<TodoItem> TodoItems { get; set; }
        public DbSet<TodoList> TodoLists { get; set; }
    }
}