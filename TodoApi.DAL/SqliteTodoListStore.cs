using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TodoApi.Model;

namespace TodoApi.DAL
{
    public class SqliteTodoListStore : ITodoListStore
    {
        private TodoContext context;

        public SqliteTodoListStore(TodoContext context)
        {
            this.context = context;
        }

        public TodoList AddTodoList(TodoList list)
        {
            context.Add<TodoList>(list);
            if (context.SaveChanges() > 0)
            {
                return list;
            }
            return null;
        }

        public bool DeleteTodoList(int id)
        {
            // delete items first
            context.RemoveRange(
                context.TodoItems.Where(ti => ti.ListId == id)
            );
            // then delete list
            context.Remove<TodoList>(context.TodoLists.SingleOrDefault(tl => tl.ListId == id));
            return context.SaveChanges() > 0;
        }

        public TodoList GetTodoList(int id)
        {
            return context.TodoLists
                .Include(tl => tl.TodoItems)
                .SingleOrDefault(tl => tl.ListId == id);
        }

        public IEnumerable<TodoList> GetTodoLists()
        {
            return context.TodoLists
                .Include(tl => tl.TodoItems)
                .AsEnumerable();
        }

        public TodoList UpdateTodoList(TodoList list)
        {
            context.Attach<TodoList>(list);
            if (context.SaveChanges() > 0)
            {
                return list;
            }
            return null;
        }
    }
}